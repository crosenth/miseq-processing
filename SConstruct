import os
import sys
from os import path, environ
import itertools
import subprocess
import csv
from collections import defaultdict, namedtuple
import signal
import json

venv = environ.get('VIRTUAL_ENV')
if not venv:
    sys.exit('--> an active virtualenv is required'.format(venv))

with open('VENV_VERSION') as f:
    venv_version = f.read().strip()

if not venv.endswith('{}-env'.format(venv_version)):
    msg = '--> use virtualenv version {} specified in ./VENV_VERSION'
    sys.exit(msg.format(venv_version))

# requirements installed in the virtualenv
from SCons.Script import (
    ARGUMENTS, Variables, Decider, Depends, EnumVariable,
    PathVariable, Help, BoolVariable, Precious, Dir, SConsignFile, Alias,
    Flatten, AlwaysBuild)

from bioscons.fileutils import Targets
from bioscons.slurm import SlurmEnvironment

# check timestamps before calculating md5 checksums
Decider('MD5-timestamp')

vars = Variables()

# declare variables for the environment
vars.Add('plate', 'miseq plate number', None)
vars.Add('nproc', 'Number of concurrent processes', default=10)
vars.Add('limit', 'Max number of specimens per batch', default=None)
vars.Add('batches', 'Comma-delimited list of batch names', default=None)

# variables providing params for the analysis
vars.Add('barcodecop_opts', default='--match-filter --qual-filter')
vars.Add('trim_left', default=15)
vars.Add('f_trunc', default=280)
vars.Add('r_trunc', default=250)
vars.Add('truncq', default=2)
vars.Add('max_mismatch', default=1)
vars.Add('nplots', default=4)
vars.Add('outdir',
         help='subdirectory of basedir in which to save output',
         default='dada2')

# Provides access to options prior to instantiation of env object
# below; it's better to access variables through the env object.
varargs = dict({opt.key: opt.default for opt in vars.options}, **vars.args)
truevals = {True, 'yes', 'y', 'True', 'true', 't'}
nproc = varargs['nproc']
nplots = int(varargs['nplots']) if varargs['nplots'] else 4

# for development and debugging, use something like limit=2 batches=1,2
limit = int(varargs['limit']) if varargs['limit'] else None
batches = set(varargs['batches'].split(',')) if varargs['batches'] else None

if not varargs['plate']:
    sys.exit('*** Specify a plate number with "scons plate=n" ***')

"""#####################################################################
########################  input data  ##################################
#####################################################################"""

basedir = '/fh/fast/fredricks_d/bvdiversity/data/miseq-plate-${plate}'

# datadir = path.join(
#     basedir, 'run-files', '*', 'Data', 'Intensities', 'BaseCalls')
# datadir = path.join(basedir, 'run-files', '*', 'Project_*', '*')
datadir = path.join(basedir, 'run-files')
fastq_pattern = '[Mm]*.fastq.gz'

sample_info_xlsx = path.join(
    basedir, 'sample-information', 'sample-information-m${plate}.xlsx')
outdir = path.join(basedir, varargs['outdir'])
plots_per_batch = 3

singularity = '/app/easybuild/software/Singularity/2.5.2-GCC-5.4.0-2.26/bin/singularity'

dada2_img = (
    '$singularity exec '
    '--bind $cwd '
    '--bind $basedir '
    '--pwd $cwd '
    '/fh/fast/fredricks_d/bvdiversity/singularity/dada2-1.10-singularity2.4-dist.simg Rscript'
)

r_img = (
    '$singularity exec '
    '--bind $cwd '
    '--bind $basedir '
    '--pwd $cwd '
    '/fh/fast/fredricks_d/bvdiversity/singularity/r-reporting-0.3.img Rscript'
)

"""#####################################################################
#########################  end input data  #############################
#####################################################################"""


def list_files(target, source, env):
    with open(str(target[0]), 'w') as f:
        f.write('\n'.join(str(x) for x in source) + '\n')


# Explicitly define PATH, giving preference to local executables; it's
# best to use absolute paths for non-local executables rather than add
# paths here to avoid accidental introduction of external
# dependencies. Environment variables are inherited from the parent
# shell from which scons is run.
environment_variables = dict(
    os.environ,
    PATH=':'.join([
        'bin',
        path.join(venv, 'bin'),
        '/app/bin',  # provides R
        '/usr/local/bin',
        '/usr/bin',
        '/bin']),
    OMP_NUM_THREADS=nproc,
    GREP_OPTIONS='--color=never',
    DADA2_NPROC=nproc,
)

env = SlurmEnvironment(
    ENV=environment_variables,
    variables=vars,
    use_cluster=False,
    SHELL='bash',
    dada2_img=dada2_img,
    r_img=r_img,
    singularity=singularity,
    basedir=basedir,
    cwd=os.getcwd(),
)

env['out'] = env.subst(outdir)
env['data'] = datadir

# save sconsign file in the output directory, not here
SConsignFile(env.subst(path.join(outdir, '.sconsign-miseq-processing')))

env.Default(env['out'])


def get_specimen_name(pth):
    return path.basename(pth).split('_')[0].lower()


# sample info from excel spreadsheet
sample_info_csv = env.Command(
    target='$out/sample_info.csv',
    source=sample_info_xlsx,
    action=('in2csv $SOURCE | '
            'csvcut -c sampleid,sample_name,project,batch,controls | '
            'csvgrep -c sampleid -r "^$" -i | '
            'sed -e "1 s/sampleid/specimen/" -e "s/sample_name/label/" '
            '> $TARGET')
)
Alias('sample_info', sample_info_csv)

sample_info = csv.DictReader(
    subprocess.check_output(
        ['in2csv', env.subst(sample_info_xlsx)]).splitlines())

NO_BATCH = 'undefined'
batchdict = {d['sampleid']: d['batch'] or NO_BATCH for d in sample_info}

# find filenames of input fastq files. These might be arranged in
# several possible directoy layouts, so we'll use 'find'.
# fastqs = glob.glob(env.subst(path.join(datadir, fastq_pattern)))
find_cmd = ['find', env.subst(datadir), '-name', fastq_pattern]
print(' '.join(find_cmd))
fastqs = [line.strip() for line in subprocess.check_output(find_cmd).splitlines()]

if not fastqs:
    msg = 'Error: no data files in {}'
    print(msg.format(env.subst(path.join(datadir, fastq_pattern))))
    sys.exit(1)

# make sure that there aren't duplicates
filenames = [os.path.basename(fn) for fn in fastqs]
if sorted(filenames) != sorted(set(filenames)):
    sys.exit('there appear to be duplicate input files')

Specimen = namedtuple(
    'Specimen', ['batch', 'name', 'f', 'r', 'f_index', 'r_index'])
fastqs.sort(key=get_specimen_name)
specimens = []
for specimen_name, files in itertools.groupby(fastqs, get_specimen_name):
    files = list(files)
    fi, ri, f, r = sorted(files)
    specimens.append(Specimen(batch=batchdict.get(specimen_name, NO_BATCH),
                              name=specimen_name,
                              f=f, r=r, f_index=fi, r_index=ri))
specimens = sorted(specimens)

if limit:
    kept = []
    keepers = defaultdict(set)
    for specimen in specimens:
        if (specimen.batch not in {'undefined'} and
                len(keepers[specimen.batch]) < limit):
            keepers[specimen.batch].add(specimen.name)
            kept.append(specimen)
    specimens = kept[:]
    del kept
    del keepers

outputs = defaultdict(list)
empty = set()
for batch, grp in itertools.groupby(specimens, lambda s: s.batch):
    # batch undefined keeps failing...
    if batch == 'undefined':
        continue

    if batches and batch not in batches:
        continue

    grp = list(grp)
    e = env.Clone(
        out=path.join('$out', 'batch_{}'.format(batch)),
        batch=batch,
        specimen=None
    )
    filtered_fastq = []

    for i, specimen in enumerate(grp):
        # Determine if there are any reads in this pair and skip
        # otherwise. Piping zcat to head prevents decompression of the
        # entire file.  See
        # https://blog.nelhage.com/2010/02/a-very-subtle-bug/ for
        # explanation of the lambda expression.
        size = subprocess.check_output(
            'zcat {} | head | wc -c'.format(specimen.f),
            shell=True,
            preexec_fn=lambda: signal.signal(signal.SIGPIPE, signal.SIG_DFL)).strip()

        if size == '0':
            empty.add(specimen.name)
            print(specimen.name + ' is empty')
            continue

        filter_action = ('barcodecop '
                         '$barcodecop_opts '
                         '--outfile $TARGET '
                         '--fastq ${SOURCES[2]} '
                         '${SOURCES[:2]}')

        # temporary hack to adjust --min-pct-assignment for presumed controls
        size_mb = os.path.getsize(specimen.f)/(1024.0 * 1024.0)
        if size_mb < 1:
            filter_action += ' --min-pct-assignment {} '.format(10)

        # pass both index files to sync fwd and rev filtering
        fwd, = e.Command(
            target='$out/fq/{}_R1_.fastq.gz'.format(specimen.name),
            source=[specimen.f_index, specimen.r_index, specimen.f],
            action=filter_action)

        rev, = e.Command(
            target='$out/fq/{}_R2_.fastq.gz'.format(specimen.name),
            source=[specimen.f_index, specimen.r_index, specimen.r],
            action=filter_action)

        filtered_fastq.extend([fwd, rev])

        # only a subset of specimens are plotted
        if i < nplots:
            e['specimen'] = specimen.name
            quality_plot = e.Command(
                target='$out/qplots/qplot_${specimen}.svg',
                source=[specimen.f, specimen.r],
                action=('$dada2_img bin/plot_quality.R $SOURCES '
                        '-o $TARGET '
                        '--trim-left $trim_left '
                        '--f-trunc $f_trunc '
                        '--r-trunc $r_trunc '
                        '--title "$specimen (batch $batch)" ')
            )
            outputs['quality_plots'].append(quality_plot)

    fastq_list, = e.Command(
        target='$out/fastq_list.txt',
        source=filtered_fastq,
        action=list_files)

    filtered_md5 = e.Command(
        target='$out/filtered.md5',
        source=fastq_list,
        action=('mkdir -p $out/filtered && '
                '$dada2_img bin/dada2_filter_and_trim.R $SOURCE '
                '--trim-left $trim_left '
                '--f-trunc $f_trunc '
                '--r-trunc $r_trunc '
                '--truncq $truncq '
                '--filt-path $out/filtered '
                '&& md5sum $out/filtered/*.gz > $TARGET ')
    )

    # TODO: provide nproc as environment variable to prevent
    # re-execution if this changes
    dada2_rda, seqtab_nochim_rda = e.Command(
        target=['$out/dada2.rda', '$out/seqtab_nochim.rda'],
        source=filtered_md5,  # another sentinel file!
        action=('$dada2_img bin/dada2_dereplicate.R $out/filtered '
                '--rdata ${TARGETS[0]} '
                '--seqtab-nochim ${TARGETS[1]} '
                '--max-mismatch $max_mismatch ')
    )

    overlaps, = e.Command(
        target='$out/overlaps.csv',
        source=dada2_rda,
        action=('$dada2_img bin/dada2_overlaps.R '
                '$SOURCE --batch $batch -o $TARGET')
    )

    outputs['seqtab_nochim'].append(seqtab_nochim_rda)
    outputs['fastq_list'].append(fastq_list)
    outputs['overlaps'].append(overlaps)

combined_fastq_list = env.Command(
    target='$out/fastq_list.txt',
    source=outputs['fastq_list'],
    action='cat $SOURCES > $TARGET'
)

combined_overlaps = env.Command(
    target='$out/overlaps.csv',
    source=outputs['overlaps'],
    action='csvcat.sh $SOURCES > $TARGET'
)

# raw read counts (forward read only)
raw_read_counts, = env.Command(
    target='$out/raw_read_counts.csv',
    source=combined_fastq_list,
    action=("grep _R1_ $SOURCE | "
            "while read p; do bin/read_count.sh $$p; done "
            "> $TARGET")
)

sv_fa, sv_table, weight, specimen_map, sv_table_long = env.Command(
    target=['$out/seqs.fasta', '$out/dada2_sv_table.csv',
            '$out/weights.csv', '$out/specimen_map.csv',
            '$out/dada2_sv_table_long.csv'],
    source=outputs['seqtab_nochim'],
    action=('$dada2_img bin/dada2_write_seqs.R $SOURCES '
            '--seqs ${TARGETS[0]} '
            '--sv-table ${TARGETS[1]} '
            '--weights ${TARGETS[2]} '
            '--specimen-map ${TARGETS[3]} '
            '--sv-table-long ${TARGETS[4]} ')
)
outputs['dada2'].extend([sv_fa, sv_table, weight, specimen_map, sv_table_long])

# create multiple alignment and identify bacterial sequences using
# cmalign alignment scores
seqs_sto, seqs_scores = env.Command(
    target=['$out/seqs.sto', '$out/sv_aln_scores.txt'],
    source=['data/ssu-align-0.1.1-bacteria-0p1.cm', sv_fa],
    action=('cmalign '
            '--cpu $nproc '
            '-o ${TARGETS[0]} '
            '--sfile ${TARGETS[1]} '
            '--noprob '
            '--dnaout '
            '$SOURCES > /dev/null')
)

# names of sequences with low bit scores
not_16s = env.Command(
    target='$out/not_16s.txt',
    source=seqs_scores,
    action='read_cmscores.py $SOURCE --min-bit-score 0 -o $TARGET'
)

# store params for report
# TODO: see this action in margergene2
def write_params(target, source, env):
    with open(str(target[0]), 'w') as f:
        keys = ['barcodecop_opts', 'trim_left', 'f_trunc', 'r_trunc', 'max_mismatch', 'truncq']
        d = {key: env[key] for key in keys}
        d['call'] = sys.argv
        json.dump(d, f, sort_keys=True, indent=4)


params = env.Command(
    target='$out/dada2_params.json',
    source=None,
    action=write_params
)
AlwaysBuild(params)

report = env.Command(
    target=['$out/quality-report.html'],
    source=['quality-report.Rmd'],
    action=('OUTDIR="$out" $r_img bin/knit.R $SOURCE -o $TARGET')
)
Depends(report, Flatten([raw_read_counts, sample_info_csv, seqs_scores] + outputs.values()))
Alias('quality-report', report)
