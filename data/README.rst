===========================
 data for miseq_processing
===========================

cmalign model
=============

Downloaded from SSU-ALIGN project (http://eddylab.org/software/ssu-align/)::

  wget http://eddylab.org/software/ssu-align/ssu-align-0.1.1.tar.gz
  tar -xf ssu-align-0.1.1.tar.gz
  cmconvert -o data/ssu-align-0.1.1-bacteria-0p1.cm ssu-align-0.1.1/models/bacteria-0p1.cm

