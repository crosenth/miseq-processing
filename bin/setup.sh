#!/bin/bash
# Usage: bin/setup.sh

# Create a virtualenv, and install requirements to it.

# options configurable from the command line
GREP_OPTIONS=--color=never

test -f VENV_VERSION || (echo cannot find file VENV_VERSION; exit 1)

VENV_VERSION=$(cat VENV_VERSION)
VENV=$(basename $(pwd))-${VENV_VERSION}-env
TOPDIR=$(cd $(dirname $BASH_SOURCE) && cd .. && pwd)
REQFILE=$TOPDIR/requirements.txt

if [[ $1 == '-h' || $1 == '--help' ]]; then
    echo "Create a virtualenv and install all pipeline dependencies"
    echo "Options:"
    echo "--no-python-deps  - skip installation of python dependencies"
    exit 0
fi

while true; do
    case "$1" in
	--no-python-deps ) PYTHON_DEPS=no; shift 1 ;;
	* ) break ;;
    esac
done

set -e

mkdir -p src
test -d $VENV || virtualenv --python python2 $VENV

source $VENV/bin/activate

# use the absolute path to the virtualenv
VENV=$VIRTUAL_ENV

if [[ -n $PYTHON_DEPS ]]; then
    echo "skipping installation of python dependencies"
else
    # install packages in $REQFILE as well as the virtualenv package
    pip install -U pip virtualenv
    pip install -r "$REQFILE"
    $VENV/bin/virtualenv --quiet --relocatable "$VENV"
fi

bin/install_infernal_and_easel.sh --prefix $VIRTUAL_ENV --srcdir src
