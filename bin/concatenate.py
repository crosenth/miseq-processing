#!/usr/bin/env python

"""Concatenate reads and provide a specimen map

"""

import sys
import argparse
import csv

from swarmwrapper.swarmwrapper import Opener, fastalite


def main(arguments):

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('infiles', nargs='+')
    parser.add_argument('--seeds', type=Opener('w'))
    parser.add_argument('--map', type=Opener('w'))

    args = parser.parse_args(arguments)

    mapfile = csv.writer(args.map) if args.map else None

    for fn in args.infiles:
        # assume specimen name is enclosing directory
        specimen = fn.split('/')[-2]
        for seq in fastalite(Opener('r')(fn)):
            seqname, __ = seq.id.rsplit('_', 1)
            args.seeds.write('>{}\n{}\n'.format(seq.id, seq.seq))

            if mapfile:
                mapfile.writerow([seqname, specimen])

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

