#!/usr/bin/env python

"""Add abundance annotation to sequence names to make swarm happy.

"""

import os
import sys
import argparse

from bioy_utils import Opener

def main(arguments):

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('infile', help="Input file", type=Opener('r'))
    parser.add_argument('-o', '--outfile', help="Output file",
                        default=sys.stdout, type=Opener('w'))

    args = parser.parse_args(arguments)

    for line in args.infile:
        if line.startswith('>'):
            spl = line.strip().split(None, 1)
            spl.insert(1, '_1 ')
            line = ''.join(spl) + '\n'
        args.outfile.write(line)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

