#!/bin/bash

# Ubuntu 12.04 Requirements (available via apt-get):
# zlib1g-dev libbz2-dev libltdl-dev libtool

if [[ -z $1 ]]; then
    echo "Usage $(basename $0) version [prefix] [srcdir]"
    exit 1
fi

version=$1
prefix=$(readlink -f ${2-/usr/local})
srcdir=${3-${prefix}/src}

if $prefix/bin/pandaseq -v 2>&1 | grep -q $version; then
    echo "pandaseq version $version is already installed in $prefix/bin"
    exit 0
fi

mkdir -p $srcdir
rm -fr $srcdir/pandaseq
cd $srcdir

git clone http://github.com/neufeld/pandaseq.git pandaseq || \
    (cd pandaseq && git fetch origin && git reset --hard origin/master)
cd pandaseq
git checkout v${version}
./autogen.sh
./configure --prefix=$prefix
make
make install

# /sbin/ldconfig       # Must update the linker library cache or pandaseq won't find libpandaseq.so

# If you ever happen to want to link against installed libraries
# in a given directory, LIBDIR, you must either use libtool, and
#    specify the full pathname of the library, or use the `-LLIBDIR'
# flag during linking and do at least one of the following:
#    - add LIBDIR to the `LD_LIBRARY_PATH' environment variable
#      during execution
#    - add LIBDIR to the `LD_RUN_PATH' environment variable
#    during linking
#       - use the `-Wl,-rpath -Wl,LIBDIR' linker flag
#    - have your system administrator add LIBDIR to `/etc/ld.so.conf'

# confirm success
$prefix/bin/pandaseq -v 2>&1 | grep $version
