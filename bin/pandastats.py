#!/usr/bin/env python

"""Gather statistics from pandaseq log file

"""

import os
import sys
import argparse
import csv
from itertools import izip_longest
from collections import defaultdict

from swarmwrapper.swarmwrapper import Opener


def handle_overlaps(rows, outfile, label=None):
    writer = csv.writer(outfile)
    counts = izip_longest(*((int(i) for i in row[3].split()) for row in rows), fillvalue=0)
    for number, count in enumerate(counts, 1):
        row = [number, sum(count)]
        if label:
            row.append(label)

        writer.writerow(row)


def handle_fate(rows, outfile, label=None):
    fieldnames, values = zip(*(row[-2:] for row in rows))
    row = [fieldnames[0], sum(map(int, values))]
    if label:
        row.append(label)

    writer = csv.writer(outfile)
    writer.writerow(row)


def main(arguments):

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('logfile', help="Input file", type=Opener('r'))
    parser.add_argument('--overlaps', type=Opener('w'))
    parser.add_argument('--fate', type=Opener('w'))
    parser.add_argument('--label')

    args = parser.parse_args(arguments)

    handlers = {
        ('STAT', 'OVERLAPS'): 'overlaps',
        # fate
        ('STAT', 'NOALGN'): 'fate',
        ('STAT', 'BADR'): 'fate',
        ('STAT', 'LOWQ'): 'fate',
        ('STAT', 'SHORT'): 'fate',
        ('STAT', 'LONG'): 'fate',
        ('STAT', 'OK'): 'fate',
    }

    stats = defaultdict(list)
    for row in csv.reader(args.logfile, delimiter='\t'):
        key = tuple(row[1:3])
        if key in handlers:
            stats[key].append(row)

    for key, handler_name in handlers.items():
        try:
            outfile = getattr(args, handler_name)
        except AttributeError:
            continue

        if outfile:
            rows = stats[key]
            if rows:
                globals()['handle_{}'.format(handler_name)](rows, outfile, args.label)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

