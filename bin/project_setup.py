#!/usr/bin/env python

"""Extract miseq reads corresponding to a project PROJECT into
'../miseq-project-$PROJECT/'

"""

import os
import sys
import argparse
import glob
import csv
from os import path
from collections import defaultdict, Counter
from operator import itemgetter
from itertools import islice

from swarmwrapper.swarmwrapper import Opener, fastalite
from swarmwrapper.swarmwrapper import main as swarm


def get_sample_information(topdir):
    files = glob.glob(path.join(topdir, 'miseq-plate-*/processed/sample_info.csv'))
    info = defaultdict(list)
    for fn in files:
        thisdir = path.dirname(fn)
        plate = path.dirname(thisdir).split('-')[-1]
        with open(fn) as f:
            reader = csv.DictReader(f)
            for row in reader:
                if row['project']:
                    row['otu_reps'] = path.join(
                        thisdir, row['specimen'], 'otu_reps.fasta.gz')
                    row['plate'] = plate
                    assert path.exists(row['otu_reps']), 'missing data: ' + str(row)
                    info[row['project']].append(row)
    return info


def main(arguments):

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--name', help="Project name")
    parser.add_argument('--list', action='store_true', default=False,
                        help="List projects and specimen counts and exit")
    parser.add_argument('--threads', type=int, default=12,
                        help="Number of threads to allocate to swarm [%(default)s]")
    parser.add_argument('--sample-info-only', action='store_true', default=False,
                        help="Aggregate sample info only (sample_info.csv) and exit")

    args = parser.parse_args(arguments)

    topdir = '/fh/fast/fredricks_d/bvdiversity/data'
    project_dir = path.join(topdir, 'miseq-project-{}'.format(args.name))
    try:
        os.makedirs(project_dir)
    except OSError:
        pass

    sample_info = path.join(project_dir, 'sample_info.csv')
    map_concat = path.join(project_dir, 'specimen_map_concat.csv.gz')
    map_out = path.join(project_dir, 'specimen_map.csv')
    otu_reps_concat = path.join(project_dir, 'otu_reps_concat.fasta.gz')
    otu_reps = path.join(project_dir, 'otu_reps.fasta.gz')
    weights = path.join(project_dir, 'weights.csv')

    info = get_sample_information(topdir)
    if args.list:
        for project, specimens in info.items():
            count = Counter([s['plate'] for s in specimens]).most_common()
            print project, len(specimens), count
        sys.exit()

    if args.name not in info:
        sys.exit('project "{}" not found'.format(args.name))

    specimens = sorted(info[args.name], key=itemgetter('specimen'))
    # specimens = list(islice(specimens, 5))

    # write sample_info.csv
    print 'writing {}'.format(sample_info)
    with open(sample_info, 'w') as f:
        writer = csv.DictWriter(
            f, fieldnames=['specimen', 'label', 'project', 'batch', 'controls'],
            extrasaction='ignore')
        writer.writeheader()
        writer.writerows(specimens)

    if args.sample_info_only:
        sys.exit()

    name_specimen = {}
    with Opener('w')(map_concat) as s, Opener('w')(otu_reps_concat) as o:
        specimen_map_writer = csv.writer(s)
        for specimen in specimens:
            print specimen['specimen']
            with Opener()(specimen['otu_reps']) as seqfile:
                seqs = fastalite(seqfile)
                for seq in seqs:
                    name, weight = seq.id.rsplit('_', 1)
                    specimen_map_writer.writerow([name, specimen['specimen']])
                    o.write('>{}\n{}\n'.format(seq.id, seq.seq))
                    name_specimen[name] = specimen['specimen']

    swarm([
        '-v',
        '--threads', str(args.threads),
        'cluster', otu_reps_concat,
        '--specimen-map', map_concat,
        '--seeds', otu_reps,
        '--abundances', weights,
        '--min-mass', '2',
    ])

    with Opener()(weights) as w, Opener('w')(map_out) as m:
        writer = csv.writer(m)
        for __, name, __ in csv.reader(w):
            writer.writerow([name, name_specimen[name]])


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

