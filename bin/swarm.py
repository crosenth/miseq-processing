#!/usr/bin/env python

"""Cluster reads using swarm

"""

import os
import sys
import argparse
import subprocess
from tempfile import NamedTemporaryFile as ntf

from bioy_utils import Opener


def main(arguments):

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('infile', help="Input file", type=Opener('r'))
    parser.add_argument('-o', '--otu-reps',
                        help="OTU representatives", default='otu_reps.fasta')
    parser.add_argument('-t', '--threads', default=4, type=int)
    parser.add_argument('--uclust-file', help="uclust-tyle output")
    parser.add_argument('--tmpdir', help='optional directory name for temporary files')

    args = parser.parse_args(arguments)

    if args.otu_reps.endswith('.bz2') or args.otu_reps.endswith('.gz'):
        centroids, compression = os.path.splitext(args.otu_reps)
    else:
        centroids, compression = args.otu_reps, None

    with ntf(dir=args.tmpdir) as renamed, ntf(dir=args.tmpdir) as d0, open(os.devnull) as devnull:
        nseqs = 0
        for line in args.infile:
            if line.startswith('>'):
                nseqs += 1
                spl = line.strip().split(None, 1)
                spl.insert(1, '_1 ')
                line = ''.join(spl) + '\n'
            renamed.write(line)

        renamed.flush()

        # dereplicate
        cmd = ['swarm',
               '-w', d0.name,
               '-d', '0',
               '-t', str(args.threads),
               renamed.name]

        subprocess.call(cmd, stdout=devnull, stderr=devnull)

        # cluster
        cmd = ['swarm',
               '-w', centroids,
               '-f',
               '-d', '1',
               '-t', str(args.threads),
               d0.name]

        if args.uclust_file:
            cmd.extend(['--uclust-file', args.uclust_file])

        subprocess.call(cmd, stdout=devnull, stderr=devnull)

        if compression:
            compress_using = {'.bz2': 'bzip2', '.gz': 'gzip'}[compression]
            subprocess.call([compress_using, centroids])

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

