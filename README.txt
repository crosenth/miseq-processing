-*- mode: org -*-
#+TITLE: Notes on MiSeq projects
#+AUTHOR: Noah Hoffman
#+PROPERTY: header-args:sh :results output :shebang "#!/bin/bash"

* start here

The processing pipeline is implemented in
/fh/fast/fredricks_d/bvdiversity/data/miseq-processing - go there.

* execution environment

The execution environment is provided by a virtualenv. Virtualenvs are
versioned, with the version number specified in ./VENV_VERSION. After
creation, a virtualenv should not be modified - if dependencies need
to be added or updated, create a new one by incrementing the number in
VENV_VERSION and running =bin/setup.sh=

* initial processing

Raw data from Miseq plates are saved in =/fh/fast/fredricks_d/bvdiversity/data/miseq-plate-$plate=

#+BEGIN_SRC sh
tree -L 1 ../miseq-plate-10
#+END_SRC

#+RESULTS:
: ../miseq-plate-10
: ├── processed
: ├── run-files
: └── sample-information
:
: 3 directories, 0 files

- =run-files= contains raw data
- =sample-information= contains a description of the data:

#+BEGIN_SRC sh
in2csv ../miseq-plate-10/sample-information/sample-information-m10.xlsx | head | csvlook
#+END_SRC

#+RESULTS:
#+begin_example
|---------------+-------------+---------+---------+---------+-------+-----------|
|  sampleid     | sample_name | n_index | s_index | project | batch | controls  |
|---------------+-------------+---------+---------+---------+-------+-----------|
|  m10n701-s502 | ntc_m10p1   | n701    | s502    | BVR01   | 1     | water     |
|  m10n701-s503 | dbcnt_4258  | n701    | s503    | BVR01   | 1     | buffer    |
|  m10n701-s505 | 4258-B      | n701    | s505    | BVR01   | 1     |           |
|  m10n701-s506 | 4258-1      | n701    | s506    | BVR01   | 1     |           |
|  m10n701-s507 | 4258-2      | n701    | s507    | BVR01   | 1     |           |
|  m10n701-s508 | 4258-3      | n701    | s508    | BVR01   | 1     |           |
|  m10n701-s510 | 4258-4      | n701    | s510    | BVR01   | 1     |           |
|  m10n701-s511 | 4258-5      | n701    | s511    | BVR01   | 1     |           |
|  m10n702-s502 | 4258-6      | n702    | s502    | BVR01   | 1     |           |
|---------------+-------------+---------+---------+---------+-------+-----------|
#+end_example

Plates are processed as follows (for example):

#+BEGIN_SRC sh :eval no
cd /fh/fast/fredricks_d/bvdiversity/data/miseq-processing
source miseq-processing-03-env/bin/activate
scons plate=10
#+END_SRC

where the value of 'plate' is miseq-plate-$plate

* creating a refst

Clone mkrefpkg into bvdiversity, run bootstrap.sh, etc. For Fredrick
lab data, we generally use a version of RDP that has been updated to
include lab sequences:

  refsetdir = /shared/silo_researcher/Matsen_F/MatsenGrp/micro_refset
  refs = %(refsetdir)s/rdp_plus/rdp_11_4_plus.v1.1
  ref_blast = %(refs)s/blast
  ref_seqs = %(refs)s/seqs.fasta
  ref_info = %(refs)s/seq_info.csv
  ref_taxonomy = %(refs)s/taxonomy.csv

* preparing reads for a project

Use the script =miseq-processing/bin/project_setup.py= to collect all
reads for a project.

List of projects

#+BEGIN_SRC sh
cd /fh/fast/fredricks_d/bvdiversity/data/miseq-processing
source miseq-processing-03-env/bin/activate
bin/project_setup.py --list
#+END_SRC

#+RESULTS:
: partners 377 [('9', 377)]
: N/A 1 [('10', 1)]
: n/a 3 [('8', 3)]
: BVR01 672 [('10', 369), ('3', 164), ('2', 89), ('8', 50)]
: PPT 1519 [('7', 381), ('5', 380), ('4', 379), ('6', 366), ('10', 13)]
: multi-omics 62 [('3', 62)]
: trichomonas 158 [('3', 158)]
: Schiffer 330 [('8', 330)]

Process all plates and extract reads by providing the name of a project. Example:

#+BEGIN_SRC sh :eval no
bin/project_setup.py --name PPT
#+END_SRC

Here's the output:

#+BEGIN_SRC sh
ls /fh/fast/fredricks_d/bvdiversity/data/miseq-project-PPT
#+END_SRC

#+RESULTS:
: otu_reps_concat.fasta.gz
: otu_reps.fasta.gz
: sample_info.csv
: specimen_map_concat.csv.gz
: specimen_map.csv
: weights.csv
: yield.sh

* pplacer classification pipeline (yapp)

Create new projects in bvdiversity (by convention, name using 'project-'), eg

: git clone git@github.com:fhcrc/yapp.git project-hvtn602-plate17
: cd project-hvtn602-plate17

Once the project is created, make a new project-specific branch

: git checkout -b hvtn602-plate17

Create and update settings.conf

: cp settings-fredricks.conf settings.conf

Create the virtualenv

: bin/build_env.sh

Run the pipeline

: scons

Run the "get_hits" target

: scons get_hits=yes -j20 && scons get_hits=yes -j20

If everything looks good, commit changes and push to the project
branch. Once the repo is clean (ie, no uncommitted changes), transfer
the output to a directory in bvdiversity:

: bin/transfer_to_bvdiversity.sh

