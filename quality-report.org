#+PROPERTY: results output
#+PROPERTY: exports both
#+OPTIONS: ^:nil
#+PROPERTY: header-args:python :results output :exports results
#+PROPERTY: header-args:R :results output :exports results :session "* R-miseq-quality-report *"
#+TITLE: MiSeq quailty report

* raw read counts

Read count summary

#+BEGIN_SRC R
outdir <- Sys.getenv()['OUTDIR']
read_counts <- read.csv(file.path(outdir, 'raw_read_counts.csv'), header=FALSE, as.is=TRUE)
colnames(read_counts) <- c('file', 'count')
read_counts$specimen <- sapply(strsplit(read_counts$file, '_'), '[', 1)
sample_info <- read.csv(file.path(outdir, 'sample_info.csv'))
counts <- merge(read_counts, sample_info, by='specimen')
counts <- counts[order(counts$batch),]
#+END_SRC

#+RESULTS:

#+BEGIN_SRC R
with(read_counts, summary(count))
#+END_SRC

#+RESULTS:
:    Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
:     387   26340   34680   38000   45900  142400

Number of specimens

#+BEGIN_SRC R
nrow(read_counts)
#+END_SRC

Specimens with fewer than 10K reads

#+BEGIN_SRC R :results value :colnames yes
counts[counts$count < 10000, c('batch', 'specimen', 'count')]
#+END_SRC

#+RESULTS:
| batch   | specimen    | count |
|---------+-------------+-------|
| 2       | m3n716-s502 |  7890 |
| 2       | m3n718-s510 |  9263 |
| control | m3n706-s516 |   412 |
| control | m3n706-s517 |   387 |
| control | m3n714-s507 |   504 |
| control | m3n714-s508 |   615 |
| control | m3n714-s510 |   437 |
| control | m3n714-s511 |   521 |
| control | m3n726-s505 |   818 |
| control | m3n726-s506 |  1177 |
| control | m3n726-s507 |   563 |
| control | m3n728-s521 |   593 |
| control | m3n728-s522 |   661 |

* read quality

#+BEGIN_SRC python :results output raw :exports results
import os
import glob
from itertools import groupby
# outdir = os.environ['OUTDIR']
outdir = '/fh/fast/fredricks_d/bvdiversity/data/miseq-plate-14-0mm/dada2'

def sortby(fname):
    return fname.split('/')[-3]

plots = glob.glob(os.path.join(outdir, 'batch_*', 'qplots', '*.svg'))
plots.sort(key=sortby)

for batch, grp in groupby(plots, sortby):
    print '** {}'.format(batch)
    for pth in grp:
        dirpath, fname = os.path.split(pth)
        # make path relative to compiled html file
        print '[[file:{}]]'.format(pth.replace(outdir + '/', ''))
#+END_SRC

* project status

#+BEGIN_SRC sh
pwd
echo $OUTDIR
#+END_SRC

#+BEGIN_SRC sh
git --no-pager log -n1
#+END_SRC

#+BEGIN_SRC sh
git status
#+END_SRC
